# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'l:|=* r:|=*'
zstyle :compinstall filename '/home/jarda/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000000000000
SAVEHIST=1000000000000
setopt autocd beep extendedglob notify sharehistory inc_append_history
bindkey -e
# End of lines configured by zsh-newuser-instal
#
autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_kube() {  $KUBE_PROMPT  && kube_prompt=' %B '$(kubectl config view --minify -o jsonpath='{..namespace}')' ['$(kubectl config view --minify -o jsonpath='{...server}'| sed 's/https:\/\///g'|sed 's/:6443//g')']%b' || kube_prompt='' }
precmd_functions+=( precmd_vcs_info )
precmd_functions+=( precmd_kube )
zstyle ':vcs_info:*' enable git
NEWLINE=$'\n'
zstyle ':vcs_info:git*' formats "$NEWLINE  %F{blue}[%b #%8.8i] %u %c%f"
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' stagedstr '✚'
zstyle ':vcs_info:*' unstagedstr '●'
zstyle ':vcs_info:*' get-revision true
# NORD
# PROMPT='  %F{green}%B%~%b%f %B$vcs_info_msg_0_%b
# %F{red}%f '
# NORD
neon_yellow=$'\033[1;33m'
neon_green=$'\033[1;36m'
resetattr=$'\033[0m'
PROMPT='  ${neon_yellow}%B%~%b${resetattr} %B$vcs_info_msg_0_%b
 %F{green}%f '
RPROMPT='$kube_prompt'
setopt prompt_subst
[ -e "/etc/DIR_COLORS" ] && DIR_COLORS="/etc/DIR_COLORS"
[ -e "$HOME/.dircolors" ] && DIR_COLORS="$HOME/.dircolors"
[ -e "$DIR_COLORS" ] || DIR_COLORS=""
eval "`dircolors $DIR_COLORS`"
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
[ -f ~/.clipam.zsh ] && source ~/.clipam.zsh

alias ls='ls --color=auto'
alias grep='grep --color=auto'
export EDITOR=vim
#############CUSTOM SHITE#################

alias ovpn='sudo bash /home/jarda/.bin/vpn.sh'
alias mB='ssh-add /home/jarda/.ssh/masterBlaster'
alias dd='dd status=progress'
alias bt='bash /home/jarda/.bin/blueteeth.sh'
alias mn='mountZ'
alias mon-cli='monero-wallet-cli --wallet-file ~/Monero/wallets/scottish_thunder/scottish_thunder --log-file ~/.bitmonero/cli.log'
alias HA='export ANSIBLE_VAULT_PASSWORD_FILE=/etc/home_vault_file'
alias WA='export ANSIBLE_VAULT_PASSWORD_FILE=/etc/def_vault_file'
alias PS='kimai_track && sudo openfortivpn ra.cra.cz:4444 --cookie=$(curl https://ra.cra.cz:4444/remote/login -L --cert /home/jarda/work/CRA/cert_rq/jj.certificate --key /home/jarda/work/CRA/cert_rq/jj.key  -v 2>&1 | grep SVPNCOOKIE | cut -d " " -f 3 | sed "s/;$//g" | sed "s/SVPNCOOKIE=//g")'
alias awx='awx -k'
alias k-='export KUBE_PROMPT=false && k8s_forward kill'
alias k+='export KUBE_PROMPT=true'
alias k='kubectl'
alias kcra='export KUBECONFIG=~/.kube/cra_config'
alias kd='kcra && kubectl config use-context cra-kube-dev && k+'
alias kp='kcra && kubectl config use-context cra-kube-prod && k+'
alias kdc='kcra && kubectl config use-context cra-kube-craip-dev && k+'
alias kpc='kcra && kubectl config use-context cra-kube-craip-prod && k+'
alias kdl='k8s_forward kubernetes_dev'
alias kpl='k8s_forward kubernetes_prod'
alias chd='cd ~/repositories/personal/home_datacenter'
alias C='xclip -in -selection clipboard'
alias ga='git add .'
alias gc='git commit -m'
alias gcan='git commit --amend --no-edit'
alias gp='git push'
alias gpt='git push --tags'
alias gt='git tag'
alias gcv='glab ci view'
alias as='ansible -m shell --ssh-extra-args="-i ~/.ssh/work_key -J vmsvlansible.int.cra.cz -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l worker"'
alias vl='vault login -method=oidc'
alias cloc='tokei'
alias govc='GOVC_USERNAME=$(pass acc_stajanja) GOVC_PASSWORD=$(pass stajanja) GOVC_URL=https://vcenter.int.cra.cz govc'
alias clipam='clipam -p $(pass e_jandourek)'

##########################################
autoload -Uz promptinit
promptinit
autoload -z edit-command-line
zle -N edit-command-line
bindkey "^X^E" edit-command-line
bindkey "\e[3~" delete-char
export VAULT_ADDR=https://vault.int.cra.cz
export TOWER_HOST=https://awx.cra.cz
#. /usr/lib/python3.10/site-packages/powerline/bindings/zsh/powerline.zsh
export KUBE_PROMPT=false

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/bin/vault vault
[[ -s /etc/profile.d/autojump.zsh ]] && source /etc/profile.d/autojump.zsh
