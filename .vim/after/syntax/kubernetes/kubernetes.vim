syn keyword topLevel apiVersion metadata spec data
syn keyword notableTopLevel kind name namespace
syn keyword noteworthyTopLevel labels selector template replicas parameters tls rules annotations ports type provider secretObjects
syn keyword lowLevel containers initContainers volumes
hi def link topLevel red
hi def link notableTopLevel orange
hi def link noteworthyTopLevel yellow
hi def link lowLevel orange
