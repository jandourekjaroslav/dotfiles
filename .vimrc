set nocompatible
filetype on

set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'gcmt/taboo.vim'
Plugin '907th/vim-auto-save'
Plugin 'junegunn/fzf.vim'
Plugin 'junegunn/fzf'
Plugin 'hashivim/vim-hashicorp-tools'
Plugin 'pearofducks/ansible-vim'
Plugin 'arcticicestudio/nord-vim'
Plugin 'sainnhe/gruvbox-material'
Plugin 'dense-analysis/ale'
Plugin 'jvirtanen/vim-hcl'
Plugin 'fatih/vim-go'
Plugin 'tpope/vim-fugitive'
Plugin 'tommcdo/vim-fugitive-blame-ext'
call vundle#end()            " required

let g:auto_save = 1
let g:auto_save_events = ["CursorHold"]
let g:updatetime = 2000

filetype plugin indent on
syntax on
set smartcase
set incsearch
set autowrite
set spell spelllang=en_gb
set nu
set undofile
set undodir=/tmp
set smartindent
set tabstop=4
set wrap
set hidden
set encoding=UTF-8
set list
set lcs+=space:·,tab:>-,eol:$
set t_Co=256
command A set ft=yaml.ansible
command K set ft=kubernetes.yaml
command AL !sudo ansible-lint %
command YL !yamllint %
let $FZF_DEFAULT_COMMAND = 'find . -not -path "./.git/*"'
set termguicolors
" let g:airline_theme = 'gruvbox_material'
let g:ansible_extra_keywords_highlight = 1
let g:ansible_attribute_highlight = "a"
let g:ansible_attribute_highlight = "a"
let g:ale_ansible_ansible_lint_executable = "/home/jarda/.bin/ansible-lint"
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'terraform': ['terraform'],
\   'yaml': ['prettier'],
\   'json': ['prettier'],
\   'markdown': ['prettier'],
\   'ansible': ['prettier'],
\   'yaml.ansible': ['prettier'],
\}
let g:ale_fix_on_save = 1
let g:airline#extensions#ale#enabled = 1
let g:ale_virtualtext_cursor = 'disabled'
augroup my_colours
  autocmd!
  autocmd ColorScheme gruvbox-material hi SpellBad cterm=underline
augroup END
silent! colo gruvbox-material

nnoremap ,ansible-skeleton :-1read ~/.local/share/snippets/ansible/ansible-skeleton.yml<CR>:A<CR>GddA
nnoremap ,gitlabci-skeleton :-1read ~/.local/share/snippets/ci-cd/gitlab.yml<CR>:A<CR>GddA
nnoremap ,kubernetes-skeleton :-1read ~/.local/share/snippets/kubernetes/kubernetes-skeleton.yaml<CR>:set ft=yaml<CR>gg
nnoremap ,kubernetes-config-map :-1read ~/.local/share/snippets/kubernetes/kubernetes-config-map.yaml<CR>:set ft=yaml<CR>
nnoremap ,kubernetes-vault-secret :-1read ~/.local/share/snippets/kubernetes/kubernetes-vault-secret.yaml<CR>:set ft=yaml<CR>
nnoremap ,kubernetes-vault-cert :-1read ~/.local/share/snippets/kubernetes/kubernetes-vault-cert.yaml<CR>:set ft=yaml<CR>
nnoremap ,kubernetes-deployment :-1read ~/.local/share/snippets/kubernetes/kubernetes-deployment.yaml<CR>:set ft=yaml<CR>
nnoremap ,kubernetes-statefull-set :-1read ~/.local/share/snippets/kubernetes/kubernetes-statefull-set.yaml<CR>:set ft=yaml<CR>
nnoremap ,kubernetes-ingress :-1read ~/.local/share/snippets/kubernetes/kubernetes-ingress.yaml<CR>:set ft=yaml<CR>
nnoremap ,kubernetes-service :-1read ~/.local/share/snippets/kubernetes/kubernetes-service.yaml<CR>:set ft=yaml<CR>
nnoremap ,kubernetes-pvc :-1read ~/.local/share/snippets/kubernetes/kubernetes-pvc.yaml<CR>:set ft=yaml<CR>
nnoremap ,mit-licence :-1read ~/.local/share/snippets/licences/MIT<CR>
nnoremap ,kns ggVG:s/!namespace!/
nnoremap ,kna ggVG:s/!default_name!/
nnoremap ,kp :s/!default_port!/
nnoremap ,m :Maps<CR>
nnoremap ,f :Files<CR>
nnoremap ,w :Files ~/repositories/work/<CR>
nnoremap <F7> :Git push<CR>
inoremap <expr> <c-f> fzf#vim#complete#path($FZF_DEFAULT_COMMAND)
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
nnoremap <silent> <C-N> ]s
nnoremap <silent> <C-P> [s
nnoremap <silent> <C-S> z=
nnoremap <silent> <C-a> :Git add .<CR>
nnoremap <silent> <C-c> :Git commit<CR>
nnoremap <silent> <C-u> :Git push<CR>
nnoremap <silent> <C-e> :vert Gdiffsplit<CR>
nnoremap <silent> <C-d> :vert Gdiffsplit HEAD~1<CR>
